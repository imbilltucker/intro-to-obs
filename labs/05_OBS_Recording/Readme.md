<!-- Update the lab title -->
# Lab 5 - OBS Recording

As easy as pressing "record"!


<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Tips

Record to MKV - will record even if crashes/won't lose in progress, and you can "remux" into an MP4

Record to a second HDD/external HDD has benefits, reduces impact and load to system



ToDo:
- record a short clip
- find the clip and watch it
- tweak some settings and try again
