<!-- Update the lab title -->
# Lab 2 - OBS Profiles and Scene Collections

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Profiles

profiles are different settings presets that you create - effect streaming and recording Settings
bitrates, resolutions, etc.
manage many different groups of recording and streaming Settings
useful if you have lots of different things
streaming to Twitch, Restream, direct recording to disk, recording tutorials, webcam settings, settings for specfic types of calls
- create new
- duplicate
- rename
- remove
- import and export

- new profile is blank and empty Settings
- for slight variations make it once duplicate, new name, then tweak it
- export to a folder as backup can be a good idea for many reasons - including setting up on another TV
- profiles effect everything in settings but NOT audio Settings
- recommended to have a scene collection for each profile

ToDo:
- Create a new profile for this classes
- know how to duplicate, rename, remove, import, and export
- run the profile autoconfiguration wizard


### Easy Profile Setup with Auto Configuration Wizard

recommended for this class!

Tools -> Auto-Configuration wizard

Choose to optimize streaming or Recording
Set video settings for base resolution and FPS
1080 is probably best, but may have performance improvement at 720
Choose FPS - 30 might be enough if you aren't streaming games
You must enter stream information as part of this wizard!


<!-- Update the lab title -->

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Scene Collections


scene Collections
saved collections of sets of Sources
can name scene collection and profile the same thing
scene collections for one resolution may not look right on a profile with a different resolutions
can back these up and restore and migrate to other machines, etc.


##ToDo:
- create a scene collection for the workshop
- comfortable with duplicating, creating new, exporting, renaming, importing
