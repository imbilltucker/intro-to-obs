<!-- Update the lab title -->
# Lab 0 - Install OBS

Download and install OBS on your platform of choice.
There are Linux, Widows, and Mac versions available.

Windows and Mac download and run the installer from this link:
https://obsproject.com/download

Linux:
The Linux release is available officially for Ubuntu 18.04 and newer. FFmpeg is required.

sudo apt install ffmpeg

After installing FFmpeg, install OBS Studio using:

sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt install obs-studio


Full install instructions are available here:
https://obsproject.com/wiki/install-instructions

There are options to download the source code and build your own - if you want to do that go for it, but we won't be covering that for this workshop.

After installation when you run OBS for the first time an Auto Configuration Wizard will pop up, you can either say "yes" and walk through it, or say "no" and we'll touch on that again in Lab 02.

Note: I have mainly used Windows so some of my solutions may not work on all operating systems (SplitCam for example is only available on Windows)


### Set up a streaming service

Twitch, Youtube, Facebook, Twitter/Periscope, and many other streaming services can be connected to OBS so you can stream. Youtube does have a 24 hour set up process (minimum) so if you have not enabled streaming/creator on YouTube previously that will not be the ideal option today.

Twitch, Facebook, and Periscope should all be quick to set up and enable.

Make sure you have a streaming service set up and find your Stream Key.
