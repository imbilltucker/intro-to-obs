<!-- Update the lab title -->
# Lab 3 - OBS Scenes and Sources

talk about Scenes and Sources A LOT when we talk about OBS
scenes and sources go together and are really important
Scenes are essentially collections of sources laid out in a particular way
You can then rapidly switch between scenes, layer scenes, and edit on the fly
Gives you a lot of control
<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Sources
sources are all the data sources that you are using for your stream
- audio input and output capture lets you add sound only for certain scenes (can use VLC to play to specific source)
- browser source - helps with live streaming to get pop-up elements - add browser with an online tool (i.e. streamlabs)
- color source is a solid color - backgrounds and advanced masking
- display capture is direct feed from monitor (capture cursor doesn't always work)
- game capture - good for capturing games or full screen applications
- image source is good for adding images, overlays, watermarks, etc.
- image slide show is good for slide shows! cycle between slides/images
- realsense green screen works if you have it installed
- media source for media or video file (media source or VLC media source basically the same, but may work better or worse depending on the specific content) - can use this for looping (can enable audio monitoring using this too)
- use a scene as a source for a scene - seems confusing, but very important, can be used in advanced and complicated ways
  - scene nesting helps create and combine overlays into one scene - allows you to make changes that impact everything all at once
  - helps a lot with facecams and other sources so crops, filters, and more don't effect the source everywhere - so if I crop the source in one scene it impacts the others - but by having a scene and using that it helps with cropping
- text sources adds text to the scene - and can auto load text from a text file (can use for now playing, or other things)
- window capture - captures windows - when you only want to show one specific window (individual program, etc.) and nothing else - can use to show certain things - shows same view even when you move it around the screen (note: I have had some issues with it)
- video capture device - external video devices, usually USB - webcams or capture cards/devices - set custom resolution


### Use multiple Scenes and Scene nesting

makes things look more professional, allows you to have tons more options and do even more!


### Use WebM as your source video

- WebMBRO is pretty good
- FFMPG also works


### Audio and Mixing

This is going to be pretty light, I am not an expert, but it's important to be aware of these settings because many people don't know and you need these/need to use these for some of the stuff we'll talk about.



##ToDo:
- set up a scene with a single video source (webcam)
- set up a scene with a single desktop capture (screen 1)
- If you have a second screen set up that desktop capture
- try game capture on a full screen application
- use color source as a background
- add some images (overlays or otherwise)
- set up a "starting soon" scene
- set up a "be right back" or "away" scene
- set up an "ending soon" scene
- experiment with a slide show
- create some nested scenes with multiple sources
  - webcam + information
  - webcam + game/full screen app share
  - screen share + overlays
  - what other ideas do you have? possibilities are limitless
- try adding text to a scene (scrolling perhaps? maybe a text file scripted to update by a trigger?)
- try a browser source
- try a window capture
- *bonus* media sources in VLC or converting something small to webm format
