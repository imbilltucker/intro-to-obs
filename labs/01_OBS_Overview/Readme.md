<!-- Update the lab title -->
# Lab 1 - OBS Overview

"OBS (Open Broadcaster Software) is free and open source software for video recording and live streaming." - OBSProject.com

"Open Broadcaster Software is a free and open-source cross-platform streaming and recording program built with Qt and maintained by the OBS Project. As of 2016, the software is now referred to as OBS Studio. There are versions of OBS Studio available for Microsoft Windows, macOS, and Linux distributions." - Wikipedia
<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Introduction

<!-- 3-4 sentences -->
<!-- Answer the following questions -->

OBS is very powerful software, but before we can do anything we need to get familiar with the user interface. Knowing where to access and change various settings will make your experience with OBS much better.

- What are we going to do in this lab?
  - Looking over the UI and settings in OBS
- How does it fit into the larger picture for this class?
  - Set us up for success

---

<!-- Update this section name -->
### Main OBS Window

- Contains
  - scene preview
  - scenes
    - groupings of different sources for different points of your stream
  - sources
    - individual onstream elements like video source, images, and more
  - audio mixer
    - balance different audio sources
    - monitor and manage audio Levels
    - channel assignments (recording tracks to a file)
      - channel assigned to audio source - many miss THIS
      - without changing this everything sounds the same (more later)
  - transition controls
  - start and stop buttons
- Top has normal menus just like any other program!
- bottom bar shows recording and streaming timers, CPU, and FPS
- (if CPU load is high may need to lower settings)
  - studio mode shows 2 previews and lets you prep scenes
- Settings menu
  - system wide and user interface Settings
  - themes
  - output - auto recording
  - more!
  - stream tab sets up Streaming service and log-in info
  - output configure recording and streaming Settings - recommend turn on advanced mode
  - audio tab manages audio Settings
  - video sets global video dimensions and information
  - hotkeys to customize
  - advanced tab has more high level Settings

- everything in OBS is modular so go to "view, docks, uncheck lock UI" and you can move anything anywhere you want it


##ToDo:
- Know where the UI elements are and how to navigate them
