# Intro to OBS - How to Start Streaming or Step Up Your Remote Meetings

Start broadcasting, recording, or streaming a more professional video production. Make your meetings and online workshops shine with increased production value. This hands on intro will get you started in OBS - Open Broadcaster Software.

---

### Class Goal

<!-- 4-6 sentences -->
Students should set up a basic multi-scene OBS set up, be able to stream to their service of choice, know how to record, and produce content.

---

### Success Metric

<!-- 1-2 sentences -->
Open to the possibilities of OBS, and OBS should feel less intimidating - at the end you should be ready to keep exploring and making it your own!

---

### Prerequisites:

<!-- Update this list to specify all the requirements for your class. -->
<!-- Define your expectations for the baseline student.  -->
<!-- Note: Using https://repl.it can significantly simplify requirements for programming-related classes. -->

- Install OBS on your machine and OS of choice
- Install Splitcam (recommended, not required)
- Webcam
- Headset OR USB Microphone + earphones OR speakerphone (i.e. Jabra Speak) highly recommended. Please do not use built in speakers and microphone!

(Useful but not required)
- Affinity Designer, Photoshop, or other graphic editing tool (will not be teaching how to use it, but may be useful)
- If desired can purchase overlay and transition packages for Twitch streamers from places like NerdOrDie.com - some free packages are available. THIS IS DEFINTIELY NOT REQUIRED and you may prefer to explore OBS and what it can do before acquiring a package like this (I will demonstrate how this works.)

---

<!-- Feel free to include/drop/modify this section at your discretion -->

### Following Instructions

Please follow along, and ideally don't skip ahead. That being said OBS is pretty forgiving, and the best way to learn it is to explore and make it suit you and your needs. This is a introductory class and the topics are not too advanced, but hopefully will provide you with all the information you need to explore on your own in the future.

---

# Labs

<!-- OPTIONAL: Create a Table of Contents. -->
<!-- Make sure to keep this up to date as you make edits. -->
<!-- If you opt not to do this, a link to the first lab is helpful. -->

### 0. [Verify Installation](labs/00_verify_installation/Readme.md)

### 1. [Overview](labs/01_OBS_Overview/Readme.md)

### 2. [Profiles and Scene Collections](labs/02_OBS_Profiles_and_Scene_Collections/Readme.md)

### 3. [Scenes and Sources](labs/03_OBS_Scenes_and_Sources/Readme.md)

### 4. [Studio Mode](labs/04_OBS_Studio_Mode/Readme.md)

### 5. [Recording](labs/05_OBS_Recording/Readme.md)

### 6. [Streaming](labs/06_OBS_Streaming/Readme.md)

### 7. [Bonus Material](labs/07_Bonus_Material/Readme.md)

---

<!-- Feel free to add additional materials -->

<!-- Examples:
     Recommended Reading
     General course notes
     Other links and reference materials
-->

---

### Contributing

Feel free to submit an Issue or open a Merge Request against this repository if you notice any mis-spellings, glaring omissions, or opportunities for improvement.
